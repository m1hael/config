import { TestBed } from '@angular/core/testing';

import { AppListEventService } from './applist-event.service';

describe('AppListEventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppListEventService = TestBed.get(AppListEventService);
    expect(service).toBeTruthy();
  });
});
