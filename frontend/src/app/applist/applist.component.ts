import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfigApplication } from '../configApplication';
import { ConfigService } from '../config.service';
import { ConfigEntry } from '../configEntry';
import { ClientService, ClientEvent, Client } from '../client.service';
import { AppListEventService, AppListEvent } from '../applist-event.service';

@Component({
    selector: 'app-applist',
    templateUrl: './applist.component.html',
    styleUrls: ['./applist.component.css']
})
export class ApplistComponent implements OnInit, OnDestroy {

    @Output() configSelectedRequest = new EventEmitter<ConfigApplication>();
    selectedConfigApp : ConfigApplication;
    
    configApps : ConfigApplication[];
    private clientEventSubscription;
    private applistEventSubscription;
    
    constructor(
        private toastr: ToastrService, 
        private configService: ConfigService, 
        private clientService : ClientService, 
        private applistEventService : AppListEventService) { 
        
    }

    ngOnInit() {
        this.getConfigApplications();
        
        this.clientEventSubscription = this.clientService.eventBroker.subscribe({
            next: (event : ClientEvent) => {
                this.refresh();
            }
        });
        
        this.applistEventSubscription = this.applistEventService.eventBroker.subscribe({
            next: (event : AppListEvent) => {
                if (event.action === "refresh") {
                    this.refresh();
                }
                else if (event.action === "add") {
                    if (!this.doesApplicationExist(event.payload)) {
                        this.refresh();
                    }
                }
                else if (event.action === "delete") {
                    this.refresh();
                }
            }
        });
    }

    ngOnDestroy() {
        this.clientEventSubscription.unsubscribe();
        this.applistEventSubscription.unsubscribe();
    }
    
    onSelect(config: ConfigApplication): void {
        this.selectedConfigApp = config;
        this.configSelectedRequest.emit(config);
    }
    
    refresh(): void {
        this.getConfigApplications();
    }
    
    getConfigApplications(): void {
        this.configService.getConfigApplications(this.clientService.getSelectedClient().id).subscribe(configApps => {
            this.configApps = configApps;
            this.configApps.sort(this.compareApp);
        });
    }
    
    private doesApplicationExist(entry : ConfigEntry) {
        if (!entry) {
            return false;
        }
        
        for (var i = 0; i < this.configApps.length; i++) {
            if (entry.application === this.configApps[i].name) {
                return true;
            }
        }
        
        return false;
    }
    
    private compareApp(a1 : ConfigApplication , a2 : ConfigApplication) {
        return a1.name.localeCompare(a2.name);
    }
}
