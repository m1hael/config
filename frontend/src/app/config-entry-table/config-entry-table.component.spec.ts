import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigEntryTableComponent } from './config-entry-table.component';

describe('ConfigEntryTableComponent', () => {
  let component: ConfigEntryTableComponent;
  let fixture: ComponentFixture<ConfigEntryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigEntryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigEntryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
