import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClientService, ClientEvent, Client } from '../client.service';
import { ConfigService } from '../config.service';
import { ConfigEntry } from '../configEntry';
import { ConfigApplication } from '../configApplication';
import { ConfigEditDialogComponent } from '../config-edit-dialog/config-edit-dialog.component';
import { ConfigAddDialogComponent } from '../config-add-dialog/config-add-dialog.component';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { AppListEventService, AppListEvent } from '../applist-event.service';

@Component({
    selector: 'app-config-entry-table',
    templateUrl: './config-entry-table.component.html',
    styleUrls: ['./config-entry-table.component.css']
})
export class ConfigEntryTableComponent implements OnInit, OnDestroy {

    @Input() configEntries: ConfigEntry[];
    @Input() configApplication: ConfigApplication;
    private applistEventSubscription;
    private clientEventSubscription;

    constructor(
        private toastr: ToastrService, 
        private modalService: NgbModal, 
        private configService: ConfigService, 
        private applistEventService : AppListEventService, 
        private clientService : ClientService, ) { 
        
    }

    ngOnInit() {
        this.clientEventSubscription = this.clientService.eventBroker.subscribe({
            next: (event : ClientEvent) => {
                this.configEntries = undefined;
                this.configApplication = undefined;
            }
        });
        
        this.applistEventSubscription = this.applistEventService.eventBroker.subscribe({
            next: (event : AppListEvent) => {
                if (event.action === "delete") {
                    if (event.payload.application === this.configApplication.name) {
                        this.configEntries.splice(this.configEntries.indexOf(event.payload), 1);
                    }
                }
                else if (event.action === "add") {
                    this.configEntries.push(event.payload);
                }
            }
        });
    }
    
    ngOnDestroy() {
        this.clientEventSubscription.unsubscribe();
        this.applistEventSubscription.unsubscribe();
    }

    newEntry() : void {
        var entry = new ConfigEntry();
        entry.application = this.configApplication.name;
        const modalRef = this.modalService.open(ConfigAddDialogComponent);
        modalRef.componentInstance.entry = entry;
    }

    edit(entry: ConfigEntry) : void {
        const modalRef = this.modalService.open(ConfigEditDialogComponent);
        modalRef.componentInstance.entry = entry;
    }
    
    delete(entry : ConfigEntry) : void {
        const modalRef = this.modalService.open(ConfirmDialogComponent);
        modalRef.componentInstance.entry = entry;
    }
}
