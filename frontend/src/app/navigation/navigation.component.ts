import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClientService, ClientEvent, Client } from '../client.service';
import { ConfigService } from '../config.service';
import { ConfigEntry } from '../configEntry';
import { ConfigAddDialogComponent } from '../config-add-dialog/config-add-dialog.component';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

    title = 'Configuration';
    clients : Client[];
    
    private eventSubscription;
    
    constructor(private toastr: ToastrService, private modalService: NgbModal, private configService: ConfigService, private clientService: ClientService) { }
  
    ngOnInit() {
        this.clientService.getClients().subscribe((clients) => { this.clients = clients; });
    }

    showAddConfigDialog(): void {
        const modalRef = this.modalService.open(ConfigAddDialogComponent);
    }
    
    selectClient(client : Client) : void {
        this.clientService.selectClient(client);
    }
}
