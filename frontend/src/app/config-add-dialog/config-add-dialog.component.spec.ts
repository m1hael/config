import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigAddDialogComponent } from './config-add-dialog.component';

describe('ConfigAddDialogComponent', () => {
  let component: ConfigAddDialogComponent;
  let fixture: ComponentFixture<ConfigAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
