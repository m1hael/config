import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ConfigEntry } from '../configEntry';
import { ConfigService } from '../config.service';
import { ClientService, ClientEvent, Client } from '../client.service';
import { AppListEventService, AppListEvent } from '../applist-event.service';

@Component({
  selector: 'app-config-add-dialog',
  templateUrl: './config-add-dialog.component.html',
  styleUrls: ['./config-add-dialog.component.css']
})
export class ConfigAddDialogComponent implements OnInit {

    constructor(
        public modal : NgbActiveModal, 
        private toastr: ToastrService, 
        private configService: ConfigService, 
        private clientService : ClientService, 
        private applistEventService : AppListEventService) {
        
    }

    ngOnInit() { }

    entry = new ConfigEntry();

    onSubmit() {
        if (this.entry.index !== undefined) {
            this.entry.index = parseInt(this.entry.index.toString());
        }
        this.configService.saveEntry(this.clientService.getSelectedClient().id, this.entry).subscribe((response) => {
            if (response.status == 200) {
                this.toastr.info("Configuration saved.");
                this.modal.dismiss('entry saved');
                this.applistEventService.eventBroker.emit(new AppListEvent("add", this.entry));
            }
            else {
                this.toastr.error("Configuration not saved.");
            }
        });        
    }
}
