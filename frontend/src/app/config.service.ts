import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ConfigApplication } from './configApplication';
import { ConfigEntry } from './configEntry';

@Injectable({
    providedIn: 'root'
})
export class ConfigService {

    url = "http://opensrc.rzkh.de:44044/config";

    constructor(private http: HttpClient) { }

    getConfigApplications(client : number): Observable<ConfigApplication[]> {
        return this.http.jsonp<ConfigApplication[]>(this.url + "?client=" + client, "callback");
    };
    
    getConfigEntries(client:number, application:string): Observable<ConfigEntry[]> {
        return this.http.jsonp<ConfigEntry[]>(this.url + "/" + application + "?client=" + client, "callback").pipe(
            tap( entries => { 
                for (var i = 0; i < entries.length; i++) {
                    entries[i].application = application;
                }
            } )
        );
    };
    
    private handleError<T>(operation = 'operation', result? : T) {
        return (error : any) : Observable<T> => {
            console.log("handle failed " + operation);
            return of(result as T);
        };
    };
    
    saveEntry(client:number, entry:ConfigEntry): Observable<HttpResponse<ConfigEntry>> {
        let url = this.url + "/" + entry.application + "/entry?crossorigin=anonymous&client=" + client;
        return this.http.put<ConfigEntry>(url, entry, { observe: 'response' }).pipe(
            catchError(this.handleError<HttpResponse<ConfigEntry>>('saveEntry'))
        );
    };
    
    deleteEntry(client:number, entry:ConfigEntry) : Observable<HttpResponse<ConfigEntry>> {
        let url = this.url + "/" + entry.application + "/entry?crossorigin=anonymous&client=" + client + 
            "&index=" + entry.index + "&key=" + entry.key + "&application=" + entry.application;
        return this.http.delete<ConfigEntry>(url, { observe: 'response' }).pipe(
            catchError(this.handleError<HttpResponse<ConfigEntry>>('deleteEntry'))
        );
    }; 
}
