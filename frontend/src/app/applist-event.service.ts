import { Injectable, EventEmitter } from '@angular/core';

export class AppListEvent {
    action : string = "refresh";
    payload;
    
    constructor(action?, payload?) {
        this.payload = payload;
        
        if (action) {
            this.action = action;
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class AppListEventService {

    public eventBroker : EventEmitter<AppListEvent> = new EventEmitter<AppListEvent>();

    constructor() { }
}
