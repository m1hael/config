import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigService } from '../config.service';
import { ConfigEntry } from '../configEntry';
import { ClientService, ClientEvent, Client } from '../client.service';
import { AppListEventService, AppListEvent } from '../applist-event.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {

    constructor(public modal : NgbActiveModal, private toastr: ToastrService, private configService: ConfigService, private clientService : ClientService, private applistEventService : AppListEventService) { }

    ngOnInit() {
    }

    entry : ConfigEntry;

    delete() : void {
        this.configService.deleteEntry(this.clientService.getSelectedClient().id, this.entry).subscribe((response) => {
            if (response.status == 200 || response.status == 204) {
                this.toastr.info("Configuration deleted");
                this.modal.dismiss('entry deleted');
                this.applistEventService.eventBroker.emit(new AppListEvent("delete", this.entry));
            }
            else {
                this.toastr.error("Konfiguration nicht gelöscht.");
            }
        });
    };
}
