import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule }    from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { ApplistComponent } from './applist/applist.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SplashComponent } from './splash/splash.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ConfigEntryTableComponent } from './config-entry-table/config-entry-table.component';
import { ConfigEditDialogComponent } from './config-edit-dialog/config-edit-dialog.component';
import { ConfigAddDialogComponent } from './config-add-dialog/config-add-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ApplistComponent,
    SplashComponent,
    NavigationComponent,
    ConfigEntryTableComponent,
    ConfigEditDialogComponent,
    ConfigAddDialogComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  entryComponents: [
    ConfigAddDialogComponent,
    ConfigEditDialogComponent,
    ConfirmDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
