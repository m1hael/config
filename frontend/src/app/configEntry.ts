export class ConfigEntry {

  application : string;
  key: string;
  index: number = 0;
  valueString: string;
  valueDate: string;
  valueNumber: number;
  changedOn: string;
  changedBy: string;

}