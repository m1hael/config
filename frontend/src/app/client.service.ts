import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';

export class Client {
    id : number;
    name : string;
}

export class ClientEvent {
    action : string = "selected";
    client : Client;
    
    constructor(client : Client) {
        this.client = client;
    }
}

@Injectable({
    providedIn: 'root'
})
export class ClientService {

    clients : Client[] = [
        { id : 1 , name : "RPG Next Gen" },
        { id : 2 , name : "MiWorkplace" }
    ];

    private selectedClient : Client = { id : 1 , name : "RPG Next Gen" };
    public eventBroker : EventEmitter<ClientEvent> = new EventEmitter<ClientEvent>();
    
    constructor() { }
    
    getClients() : Observable<Client[]> {
        return of(this.clients);
    }
    
    getSelectedClient() : Client {
        return this.selectedClient;
    }
    
    selectClient(client : Client) : void {
        this.selectedClient = client;
        this.eventBroker.emit(new ClientEvent(client));
    }
}
