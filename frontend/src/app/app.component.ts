import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfigApplication } from './configApplication';
import { ConfigEntry } from './configEntry';
import { ConfigService } from './config.service';
import { ClientService, Client } from './client.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    configApplication : ConfigApplication;
    configEntries : ConfigEntry[];
    
    constructor(private toastr: ToastrService, private configService: ConfigService, private clientService : ClientService) { }
    
    ngOnInit() {
    }

    showConfigEntries(config: ConfigApplication): void {
        this.configApplication = config;
        this.getConfigEntries(config.name);
    }
    
    getConfigEntries(application:string): void {
        this.configService.getConfigEntries(this.clientService.getSelectedClient().id, application)
            .subscribe(configEntries => this.configEntries = configEntries);
    }
}
