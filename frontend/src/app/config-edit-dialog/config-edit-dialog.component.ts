import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ConfigService } from '../config.service';
import { ConfigEntry } from '../configEntry';
import { ClientService, ClientEvent, Client } from '../client.service';

@Component({
  selector: 'app-config-edit-dialog',
  templateUrl: './config-edit-dialog.component.html',
  styleUrls: ['./config-edit-dialog.component.css']
})
export class ConfigEditDialogComponent implements OnInit {

  constructor(public modal : NgbActiveModal, private toastr: ToastrService, private configService: ConfigService, private clientService : ClientService) { }

  ngOnInit() {
  }
  
  entry : ConfigEntry;
  
  onSubmit() {
        if (this.entry.index !== undefined) {
            this.entry.index = parseInt(this.entry.index.toString());
        }
        this.configService.saveEntry(this.clientService.getSelectedClient().id, this.entry).subscribe((response) => {
            if (response.status == 200) {
                this.toastr.info("Configuration saved.");
                this.modal.dismiss('entry saved');
            }
            else {
                this.toastr.error("Configuration not saved.");
            }
        });        
    }
}
