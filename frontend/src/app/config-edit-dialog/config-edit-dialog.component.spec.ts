import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigEditDialogComponent } from './config-edit-dialog.component';

describe('ConfigEditDialogComponent', () => {
  let component: ConfigEditDialogComponent;
  let fixture: ComponentFixture<ConfigEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigEditDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
