# Configuration

This project is a prototype and example project for creating an exposing and ILE
service program as a REST service with an HTML5 frontend.

The service program can be used in production regardless of the REST service
and the UI for externalizing application configuration so that the application
doesn't have to be recompiled if the configuration changes.

The project uses [ILEastic][1] for creating the REST service and [Angular][2]
for creating the UI.

## Requirements

This project has dependencies to some other projects.

On the backend side we use

  - [ILEastic][1]
  - [Message][d1]
  - [JSON][d2]
  - [Arraylist][d3]

On the frontend side we use

  - [Angular][d4]
  - [Bootstrap][d5]
  - [Fontawesome][d6]
  - [RxJS][d7]
  - [Toastr][d8]

## Installation

### Source Installation

#### Backend

The project uses `make` for building the backend part of the project.

The build script expects to use the the library CONFIG for creating
the IBM i objects. If another library should be used you can specify it 
with the `BIN_LIB` parameter.

    make BIN_LIB=ALTCFGLIB

The build script expects the copy books of the dependencies in the folder
`/usr/local/include`. You can change the location of the include directory
by passing it to the parameter `INCDIR`.

    make INCDIR=/home/mihael/include

The service programs MESSAGE, JSON, ARRAYLIST and ILEASTIC must be accessable
via the library list for a successful build.

#### Frontend

For the Angular part of the project the Angular native CLI tool `ng` is used.
See the [Getting Started][7] section on the Angular.io site for how to install 
the Angular CLI tool.

For dependency management `npm`is used.

For starting the Angular embedded web server for testing the UI you can
use

    ng serve
    
For building the Angular application for the test environment use

    ng build

For building the Angular application for the production environment use

    ng build --prod


### Binary Installation

You can get a save file for the IBM i objects (for 7.3) and an archive file
for the web application at the Bitbucket [download][5] section.

Depending on the path of your installation your may need to adjust the _base href_
in the index.html file, see [Mozilla Web Docs - base element][6].

The packed web application has a fixed url to the REST service which is probably 
not the same service you want to use.

The packed REST service will serve on port 44044.


## Documentation

The API documenation of the CONFIG service program is available at the open public
documentation library [ILEDocs][3] hosted at [rpgnextgen.com][4].

## Demo

You can find a demo installation at [config.rpgnextgen.com][8]. The backend may be
offline as I don't have an IBM i server by myself to host the REST service.

## License

This service program is released under the Apache 2.0 License.


[1]: https://github.com/sitemule/ILEastic
[2]: https://angular.io
[3]: http://iledocs.rpgnextgen.com
[4]: https://www.rpgnextgen.com
[5]: https://bitbucket.org/m1hael/config/downloads/
[6]: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base
[7]: https://angular.io/guide/quickstart
[8]: http://config.rpgnextgen.com
[d1]: https://bitbucket.org/m1hael/message
[d2]: https://www.rpgnextgen.com?content=json
[d3]: https://bitbucket.org/m1hael/arraylist
[d4]: https://angular.io
[d5]: https://getbootstrap.com
[d6]: https://fontawesome.com
[d7]: http://reactivex.io/
[d8]: https://github.com/scttcper/ngx-toastr
