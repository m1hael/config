**FREE

/if not defined (CONFIG)
/define CONFIG

///
// Configuration
//
// This module simplifies the loading and managing of configuration data for ILE
// programs. The configuration is saved under the corresponding program name and 
// an additional key/index.
// <br><br>
// A configuration entry can contain a character, date and number value.
// <br><br>
// Configuration entries are seperate for each client.
// <br><br>
// Example configuration entries for a web server would be the host name and 
// the port.
// <br><br>
// Some procedure allow the passing of a default value which will be returned
// if there is no configuration entry for the passed keys. If the caller didn't
// provide a default value and there is no configuration entry for the passed
// keys an escape message will be sent.
//
// @author Mihael Schmidt
// @date 26.10.2018
// @project Configuration
// @version 1.0.0
// @link https://bitbucket.org/m1hael/config Project website
///

///
// config entry template
///
dcl-ds config_entry_t qualified template;
  application char(10);
  key char(20);
  index int(10);
  valueChar varchar(200);
  valueDate date;
  valueNumber packed(15:5);
end-ds;

///
// Get character value
//
// Returns the character value of a config entry.
//
// @param Client
// @param Application
// @param Key
// @param Index (optional)
// @param Default value (optional)
// @return Config value
// @throws Escape message if no default value has been passed and no config
//         entry exists.
///
dcl-pr config_get varchar(200) extproc(*dclcase);
  client int(10) const;
  application char(10) const;
  key char(20) const;
  index int(10) const options(*omit : *nopass);
  default varchar(200) const options(*nopass);
end-pr;

///
// Get config number value
//
// Returns the number value of a config entry.
//
// @param Client
// @param Application
// @param Key
// @param Index (optional)
// @param Default value (optional)
// @return Config value
// @throws Escape message if no default value has been passed and no config
//         entry exists.
///
dcl-pr config_getNumber packed(15:5) extproc(*dclcase);
  client int(10) const;
  application char(10) const;
  key char(20) const;
  index int(10) const options(*omit : *nopass);
  default packed(15:5) const options(*nopass);
end-pr;

///
// Get config date value
//
// Returns the date value of a config entry.
//
// @param Client
// @param Application
// @param Key
// @param Index (optional)
// @param Default value (optional)
// @return Config value
// @throws Escape message if no default value has been passed and no config
//         entry exists.
///
dcl-pr config_getDate date extproc(*dclcase);
  client int(10) const;
  application char(10) const;
  key char(20) const;
  index int(10) const options(*omit : *nopass);
  default date const options(*nopass);
end-pr;

///
// Get config entry
//
// Returns the whole configuration entry. The config entry data structure is 
// empty if no config entry exists.
//
// @param Client
// @param Application
// @param Key
// @param Index (optional)
// @return Configuration entry (config_entry_t)
///
dcl-pr config_getEntry likeds(config_entry_t) extproc(*dclcase);
  client int(10) const;
  application char(10) const;
  key char(20) const;
  index int(10) const options(*nopass);
end-pr;

///
// Get configuration entries
//
// Returns all configuration entries to the application and optionally to the
// key.
//
// @param Client
// @param Application
// @param Key (optional)
// @return List of configuration entries (config_entry_t)
//
// @info The caller has to free the memory of the list after processing.
///
dcl-pr config_listEntries pointer extproc(*dclcase);
  client int(10) const;
  application char(10) const;
  key char(20) const options(*nopass);
end-pr;

///
// Persist config entry
//
// Persist the passed config entry. An existing config entry with the same
// key will be overwritten.
//
// @param Client
// @param Config entry
///
dcl-pr config_persist extproc(*dclcase);
  client int(10) const;
  entry likeds(config_entry_t) const;
end-pr;

///
// Delete config entrie(s)
//
// Deletes the config entry. If no index has been passed all config entries
// under the client/application/key will be deleted. No escape message will
// be sent if no config entries exists to the passed key.
//
// @param Client
// @param Application
// @param Key
// @param Index (optional)
///
dcl-pr config_delete extproc(*dclcase);
  client int(10) const;
  application char(10) const;
  key char(20) const;
  index int(10) const options(*nopass);
end-pr;

///
// List applications
//
// Returns a distinct list of all applications for a specific client.
//
// @param Client
// @return List of applications (names) or *NULL if an error occured.
///
dcl-pr config_listApplications pointer extproc(*dclcase);
  client int(10) const;
end-pr;

/endif
