      /if not defined (PSDS)
      /define PSDS

     DprogramStatus   SDS                  qualified
     D procedure         *PROC
     D programStatus     *STATUS
     D previousStatus         16     20S 0
     D lineNumber             21     28
     D routine           *ROUTINE
     D parameters        *PARMS
     D exceptionType          40     42
     D exceptionNumber...
     D                        43     46
     D programLibrary         81     90
     D exceptionData          91    170
     D exceptionId           171    174
     D date                  191    198
     D year                  199    200S 0
     D lastUsedFileOnError...
     D                       201    208
     D fileErrorInfo         209    243
     D jobName               244    253
     D username              254    263
     D jobNumber             264    269S 0
     D jobDate               270    275S 0
     D runDate               276    281S 0
     D runTime               282    287S 0
     D compileDate           288    293
     D compileTime           294    299
     D compilerLevel         300    303
     D sourceFile            304    313
     D sourceLibrary         314    323
     D sourceMember          324    333
     D programName           334    343
     D moduleName            344    353
     D userProfileName...
     D                       358    367
     D externalErrorCode...
     D                       368    371I 0
     D xmlElementsSet        372    379I 0

      /endif